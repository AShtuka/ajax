class XHR {
    getAndShow = function (url, filmShow, characterShow) {
        let xhr = new XMLHttpRequest();
        xhr.open("GET", url);
        xhr.responseType = "json";
        xhr.send();
        try {
            xhr.onload = function () {
                if (xhr.status === 200) {
                    xhr.response.results.forEach((film) => {
                        filmShow(film, ".left-container");
                        film.characters.forEach(url => {
                            let innerXhr = new XMLHttpRequest();
                            innerXhr.open("GET", url);
                            innerXhr.responseType = "json";
                            innerXhr.send();
                            innerXhr.onload = function () {
                                if (innerXhr.status === 200) {
                                    characterShow(innerXhr.response.name, film.episode_id + ".left-container");
                                }
                            }
                        })
                    })
                } else {
                    const reason = new Error(`Error ${xhr.status}: ${xhr.statusText}`);
                    throw reason;
                }
            }

            xhr.onerror = function () {
                const reason = new Error("Request failed");
                throw reason;
            }

        } catch (e) {
            throw e;
        }
    }
}
    
