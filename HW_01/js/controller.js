(function () {
    let fetch = new Fetch();
    let view = new View;
    let xhr = new XHR();
    const doWorkWithXMLHttprequest = function () {
        xhr.getAndShow("https://swapi.co/api/films/", view.showFilm, view.showCharcters);
    }

    const doWorkWithFetch = function(){
        fetch.get("https://swapi.co/api/films/")
            .then((result) => {
                result.results.forEach((film) => {
                    view.showFilm(film, ".right-container");
                    Promise.all(film.characters.map(url => {
                        return fetch.get(url);
                    }))
                        .then((characters) => {
                            characters.forEach(character => {
                                view.showCharcters(character.name, film.episode_id + ".right-container");
                            })
                        })
                });
            })
    }

    doWorkWithXMLHttprequest();
    doWorkWithFetch();

}());