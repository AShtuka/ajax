class View {
    showFilm = function (film, container) {
        const filmContainer  = document.createElement("div");
        filmContainer.innerHTML = `<span class="film-title">Episode : ${film.episode_id} - </span>
                                   <span class="film-title">${film.title}</span>
                                   <p>${film.opening_crawl}</p>
                                   <p class="characters-list">Characters List</p>
                                   <ul id=${film.episode_id}${container}></ul>`;
        document.querySelector(`${container}`).appendChild(filmContainer);
    }
    
    showCharcters = function (name, id) {
        const character = document.createElement("li");
        character.innerText = name;
        document.getElementById(id).appendChild(character);
    }
}