const url = "https://api.ipify.org/?format=json";

get = async function (url) {
    let response = await fetch(url);
    if (response.ok) {
        let json = await response.json();
        return json;
    } else {
        console.log("Ошибка HTTP: " + response.status);
    }
}

createRequest = function(ip) {
    let requestStr = document.querySelector(".input-form");
    requestStr.placeholder = `http://ip-api.com/json/${ip}?lang=ru&fields=continent,country,region,city,district`
}

getIP = async function () {
    const ip = await get(url);
    await createRequest(ip.ip);
}

showInfo = function(data) {
    document.querySelector(".info").className = "info";
    document.querySelector(".continent").innerText = `"continent": "${data.continent}",`;
    document.querySelector(".country").innerText = `"country": "${data.country}",`;
    document.querySelector(".region").innerText = `"region": "${data.region}",`;
    document.querySelector(".city").innerText = `"city": "${data.city}",`;
    document.querySelector(".district").innerText = `"district": "${data.district}"`;
}

getIP();

let requestStr = document.querySelector(".input-form");
requestStr.addEventListener("focus", function () {
    requestStr.value = requestStr.placeholder;
})

let btn = document.querySelector(".btn");
btn.addEventListener("click", async function () {
    const url = document.querySelector(".input-form").value;
    const placeholder = document.querySelector(".input-form").placeholder;
    url ? showInfo(await get(url)) : showInfo(await get(placeholder));
})